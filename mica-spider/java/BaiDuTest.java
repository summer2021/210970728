
import net.dreamlu.mica.http.HttpRequest;
import net.dreamlu.mica.http.ResponseSpec;


public class BaiDuTest {
    public static void main(String[] args) {

        String html= HttpRequest.get("https://www.oschina.net")
                .execute()
                .onFailed((request, e) -> { // 网络等异常情况的消费处理，可无
                    e.printStackTrace();
                })
                .onResponse(ResponseSpec::asString);// 处理响应，有网络异常等直接返回 null
        System.out.println(html);


    }
}
