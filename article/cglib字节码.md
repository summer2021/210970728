目前字节码插装方法主要分为两类： 静态插装和动态插装。
 静态字节码插装是指被插装的程序在开始执行之前就插入了所有插装代码的插装方法； 该方法的主要优势是它消耗较少的程序额外运行时间， 因为所有的类都在程序执行前就已经插装了； 静态字节码插装还有一个优势， 就是它可以使用任何已有的上层字节码工程库， 比如 BCEL， ASM， JOIE， Javassist 等等。

而静态字节码插装的最大的缺点是它无法对动态生成或动态加载的代码进行插装。

 而动态字节码插装是在待插装程序运行的同时执行插装； 每当有一个类被加载时， 插装代理程序都会被调用，并可能在被加载的字节码中插入装代码；动态字节码插装的缺点是它会带来大量额外时间消耗，但是另一方面，它保证了所有被加载的类都会被插装。

 CGLIB，代理的是类，不需要业务类继承接口，通过派生的子类来实现代理。通过在运行时，动态修改字节码达到修改类的目的。

 ![流程时序.png](./流程时序.png)

如图所示的系统时序图的交互过程中， 客户端应用程序先创建一个ClassReader 对象实例， 然后以 ClassAdapter 实例作为参数调用该对象实例的accept()方法， 完成这个调用之后， 传入的 ClassAdapter 就可以在需要的时候遍历该 ClassReader 实例所读取的字节码。