# **cglib bean copy介绍**

## Cglib BeanCopier 介绍

阿里巴巴 p3c 插件中有这么一项检查 “避免用Apache Beanutils进行属性的copy，Apache BeanUtils性能较差，可以使用其他方案比如Spring BeanUtils, Cglib BeanCopier”

**性能比较**，下图是 github 上的一个 Bean copy 性能的对比，可以看出 Bean copy 工具性能差距还是比较大。	

![11111111.png](https://segmentfault.com/img/remote/1460000018656129?w=1766&h=790)

图中可以看出，Cglib BeanCopier 的性能十分强劲。

## 使用

Cglib 以源码的形式纳入到 Spring core 中，所以大家使用 Spring、Spring boot 可以直接使用。其它则需要自己添加依赖，下面的使用例子都以 Spring 的为主。注意：使用了 `Lombok` 。

```
@Data
public class User {
   private Integer id;
   private String name;
   private Integer age;
}
```

```
@Data
public class UserVo {
    private String name;
    private Integer age;
}
```

Bean 拷贝

```
public class UserCopyTest {
    public static void main(String[] args) {
        User user = new User();
        user.setId(250);
        user.setName("天天向上");
        user.setAge(30);

        UserVo userVo = new UserVo();
        // 3. 构造 BeanCopier，不是用类型转换
        BeanCopier copier = BeanCopier.create(User.class, UserVo.class, false);
        // 4. 拷贝对象，不是用类型转换，转换器可以使用 null
        copier.copy(user, userVo, null);

        // 5. 打印结果：UserVo(name=天天向上, age=30)
        System.out.println(userVo);
    }
}
```

## 原理

大家都知道 Cglib BeanCopier，之所以性能这么高主要是利用了 Asm 字节码技术。

在 UserCopyTest 的 main 方法中添加下面的代码（建议直接放置到 1. 初始化 user，赋值 之前），指定cglib 源码生成目录，建议生成到 idea 项目中，可以直接打开生成的 class 字节码。

```
// 设置 cglib 源码生成目录
String sourcePath ="/IDEAMyProject/Fproject/software/src/test/java/com/example/software";
System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, sourcePath);
```

再次执行 main 方法。

![image-20210812233944091](C:\Users\zou\AppData\Roaming\Typora\typora-user-images\image-20210812233944091.png)

## Cglib copy 问题

1. 不支持链式 bean，mybatis-plus 生成的 Model 中默认添加了 @Accessors(chain = **true**) 注解默认为链式。
2. 不支持 原始类型和封装类型 copy `int <-> Integer`。
3. 类型转换不够智能，设置 useConverter 为 true 和重写 Converter，`类型相同`也会走转换的逻辑













# Cglib和Mica Bean copy生成字节码对比

## Bean 模型

我们列举2个模型 `User` 和 `UserVo`，注意：`birthday` 字段类型不一样

```
@Data
public class User {
    private Integer id;
    private String name;
    private Integer age;
    private LocalDateTime birthday;
}
```

```
@Data
public class UserVo {
    private String name;
    private Integer age;
    private String birthday;
}
```

## Cglib Bean copy 字节码分析

### 配置 Cglib debug 模式

```
// 设置 cglib 源码生成目录
String sourcePath ="/IDEAMyProject/Fproject/software/src/test/java/com/example/software";
System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, sourcePath);
```

### Bean copy 时生成的字节码

 不使用类型转换时，Bean copy 的代码如下：

```
User user = new User();
user.setId(250);
user.setName("天天向上");
user.setAge(30);
user.setBirthday(LocalDateTime.now());
// 2. 初始化 userVo
UserVo userVo = new UserVo();
// 3. 构造 BeanCopier，不是用类型转换
BeanCopier copier = BeanCopier.create(User.class, UserVo.class, false);
// 4. 拷贝对象，不是用类型转换，转换器可以使用 null
copier.copy(user, userVo, null);

// 5. 打印结果：UserVo(name=天天向上, age=30, birthday=null)
System.out.println(userVo);
```

**生成的字节码：**

```
public class Object$$BeanCopierByCGLIB$$70f9539b extends BeanCopier {
    public Object$$BeanCopierByCGLIB$$70f9539b() {
    }
    public void copy(Object var1, Object var2, Converter var3) {
        UserVo var10000 = (UserVo)var2;
        User var10001 = (User)var1;
        var10000.setAge(((User)var1).getAge());
        var10000.setName(var10001.getName());
    }
}
```

```java
public class Object$$BeanCopierByCGLIB$$70f9539b extends BeanCopier {
    public Object$$BeanCopierByCGLIB$$70f9539b() {
    }

    public void copy(Object var1, Object var2, Converter var3) {
        UserVo var10000 = (UserVo)var2;
        User var10001 = (User)var1;
        var10000.setAge(((User)var1).getAge());
        var10000.setName(var10001.getName());
    }
}
```

`注意：` 由于 `birthday` 字段类型不一样，没有生成 `set`方法。

使用类型转换时，Bean copy 的代码如下：

```
User user = new User();
user.setId(250);
user.setName("天天向上");
user.setAge(30);
user.setBirthday(LocalDateTime.now());

UserVo userVo = new UserVo();
// 3. 构造 BeanCopier，不是用类型转换
BeanCopier copier = BeanCopier.create(User.class, UserVo.class, true);
// 4. 拷贝对象，不是用类型转换，转换器可以使用 null
copier.copy(user, userVo, new Converter() {
    @Override
    public Object convert(Object o, Class aClass, Object o1) {
        if (o == null) {
            return null;
        }
        // 直接使用 mica 中的类型转换工具
        return ConvertUtil.convert(o, aClass);
    }
});

// 5. 打印结果：UserVo(name=天天向上, age=30, birthday=20-5-10 下午13:25)
System.out.println(userVo);
```

**生成的字节码：**

```
public class Object$$BeanCopierByCGLIB$$70f9539a extends BeanCopier {
    private static final Class CGLIB$load_class$java$2Elang$2EInteger;
    private static final Class CGLIB$load_class$java$2Elang$2EString;

    public Object$$BeanCopierByCGLIB$$70f9539a() {
    }

    public void copy(Object var1, Object var2, Converter var3) {
        UserVo var4 = (UserVo)var2;
        User var5 = (User)var1;
        var4.setAge((Integer)var3.convert(var5.getAge(), 			          CGLIB$load_class$java$2Elang$2EInteger, "setAge"));
        var4.setBirthday((String)var3.convert(var5.getBirthday(), CGLIB$load_class$java$2Elang$2EString, "setBirthday"));
        var4.setName((String)var3.convert(var5.getName(), CGLIB$load_class$java$2Elang$2EString, "setName"));
    }

    static void CGLIB$STATICHOOK1() {
        CGLIB$load_class$java$2Elang$2EInteger = Class.forName("java.lang.Integer");
        CGLIB$load_class$java$2Elang$2EString = Class.forName("java.lang.String");
    }

    static {
        CGLIB$STATICHOOK1();
    }
}
```

`注意：` 使用类型转换后生成了 `birthday` 的 `set`，仔细观察可以看到使用了类型转换之后生成的字节码都走了**类型转换**的逻辑。

## Mica Bean copy的字节码

由于 Mica 的 Bean copy 是基于 Cglib 进行的增强查看字节码的方式和Cglib一样，设置的方式也和上面一样。

### 不使用类型转换

**Bean copy 的代码如下：**

```
User user = new User();
user.setId(250);
user.setName("天天向上");
user.setAge(30);
user.setBirthday(LocalDateTime.now());

// 2. 使用 mica 的 BeanUtil copy 方法
UserVo userVo = BeanUtil.copy(user, UserVo.class);

// 3. 打印结果：UserVo(name=天天向上, age=30, birthday=null)
System.out.println(userVo);
```

**生成的字节码：**

```
public class Object$$MicaBeanCopierByCGLIB$$aa75e50d extends MicaBeanCopier {
    public Object$$MicaBeanCopierByCGLIB$$aa75e50d() {
    }

    public void copy(Object var1, Object var2, Converter var3) {
        UserVo var4 = (UserVo)var2;
        User var5 = (User)var1;
        var4.setAge(var5.getAge());
        var4.setName(var5.getName());
    }
}
```

`注意：` 不使用类型转换时生成的字节码同 `Cglib` 一致，只是使用更加简单一些。



### 使用类型转换时

**Bean copy 的代码如下：**

```
User user = new User();
user.setId(250);
user.setName("天天向上");
user.setAge(30);
user.setBirthday(LocalDateTime.now());

// 2. 使用 mica 的 BeanUtil copyWithConvert 方法
UserVo userVo = BeanUtil.copyWithConvert(user, UserVo.class);

// 3. 打印结果：UserVo(name=天天向上, age=30, birthday=19-2-20 下午10:24)
System.out.println(userVo);
```

**生成的字节码：**

```
public class Object$$MicaBeanCopierByCGLIB$$aa75e0e7 extends MicaBeanCopier {
    private static final Class CGLIB$load_class$java$2Elang$2EString;

    public Object$$MicaBeanCopierByCGLIB$$aa75e0e7() {
    }

    public void copy(Object var1, Object var2, Converter var3) {
        UserVo var4 = (UserVo)var2;
        User var5 = (User)var1;
        var4.setAge(var5.getAge());
        var4.setBirthday((String)var3.convert(var5.getBirthday(), CGLIB$load_class$java$2Elang$2EString, "birthday"));
        var4.setName(var5.getName());
    }

    static void CGLIB$STATICHOOK1() {
        CGLIB$load_class$java$2Elang$2EString = Class.forName("java.lang.String");
    }

    static {
        CGLIB$STATICHOOK1();
    }
}
```

`注意：` 可以看到 Mica 中对生成的字节码进行了优化，对类型相同的拷贝不使用类型转换。

## 总结

在 Mica 中对 Bean Copy 进行了大量的优化，包括类型转换优化，链式Bean支持，Map支持等，







