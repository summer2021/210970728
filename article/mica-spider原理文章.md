#  mica-spider 爬虫组件

`mica-spider` 是基于 `mica-http` 的爬虫工具。

想要使用`mica-spider`还需要先阅读 `mica-http` 部分文档。

`mica-http` 是 `okhttp` 的封装，Fluent 语法的 http 工具包，语法参考 HttpClient Fluent API。

​    OkHttp是当下Android使用最频繁的网络请求框架，由Square公司开源。Google在Android4.4以后开始将源码中的HttpURLConnection底层实现替换为OKHttp，同时现在流行的Retrofit框架底层同样是使用OKHttp的。

  在使用OkHttp发起一次请求时，对于使用者最少存在OkHttpClient、Request与Call三个角色。其中OkHttpClient和Request的创建可以使用它为我们提供的Builder（建造者模式）。而Call则是把Request交给OkHttpClient之后返回的一个已准备好执行的请求。


###  DomMapper 工具

`DomMapper` 工具采用 `cglib` 动态代理和 `Jsoup` html 解析，不到 `200` 行代码实现了 `html` 转 `java Bean` 工具，爬虫必备。

主要方法有：

- DomMapper.asDocument        ： Returns body to jsoup Document.

- DomMapper.readDocument     ：将流读取为 jsoup Document，将 html 字符串读取为 jsoup Document
- DomMapper.readValue  ：读取 xml 信息为 java Bean
- DomMapper.readList  ：读取 xml 信息为 java Bean



 CssQuery注解使用

###  CssQuery 注解说明

```
public @interface CssQuery {

	/**
	 * CssQuery
	 *
	 * @return CssQuery
	 */
	String value();

	/**
	 * 读取的 dom attr
	 *
	 * <p>
	 * attr：元素对于的 attr 的值
	 * html：整个元素的html
	 * text：元素内文本
	 * allText：多个元素的文本值
	 * </p>
	 *
	 * @return attr
	 */
	String attr() default "";

	/**
	 * 正则，用于对 attr value 处理
	 *
	 * @return regex
	 */
	String regex() default "";

	/**
	 * 默认的正则 group
	 */
	int DEFAULT_REGEX_GROUP = 0;

	/**
	 * 正则 group，默认为 0
	 *
	 * @return regexGroup
	 */
	int regexGroup() default DEFAULT_REGEX_GROUP;

	/**
	 * 嵌套的内部模型：默认 false
	 *
	 * @return 是否为内部模型
	 */
	boolean inner() default false;
}
```

示例代码：

```
// 同步调用，返回 Optional，异常时返回 Optional.empty()
Optional<String> opt = HttpRequest.post(URI.create("https://www.baidu.com"))
    .bodyString("Important stuff")
    .formBuilder()
    .add("a", "b")
    .execute()
    .onSuccessOpt(ResponseSpec::asString);
    
    
// 同步，异常时直接抛出
HttpRequest.get("https://www.baidu.com/some-form")
    .execute()
    .asString();
```



